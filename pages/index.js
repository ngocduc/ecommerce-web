import React from 'react'
import Link from 'next/link'
import { Row, Col, Menu, Input, Icon } from 'antd'
import styled from 'styled-components';


import Layout from '../components/layout'
import Card from '../components/card'

const Home = () => (
  <Layout>
    <Row>
      <Col xs={0} sm={0} md={4} lg={6} xl={6}></Col>
      <Content xs={24} sm={24} md={20} lg={18} xl={18}>
        <Row gutter={[{ xs: 8, sm: 40, md: 40, lg: 32 }, 20]}>
          {new Array(10).fill('').map((i, index) => {
            return <ItemRender key={String(index)} xs={24} sm={12} md={8} lg={8} xl={6}>
              <Card>
                <div style={{
                  background: '#ddd', height: '100%'
                }}> </div>
              </Card>
            </ItemRender>
          })}
        </Row>
      </Content>

    </Row>
  </Layout>
)

const ItemRender = styled(Col)`
  position: relative;
  &:after {
    content: "";
    display: block;
    padding-bottom: 100%;
  }

`;

const Content = styled(Col)`
  padding: 30px 20px;
`

export default Home
