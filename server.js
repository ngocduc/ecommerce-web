const express = require('express')
const next = require('next')
const mysql = require('mysql');


const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare()
.then(() => {
  const server = express()

  server.use(express.json()); 
  server.use(express.json());       // to support JSON-encoded bodies
  server.use(express.urlencoded());
  
  server.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  var connectsql
  function handleDisconnect()
  {
    connectsql = mysql.createConnection(
      {
        host:'localhost',
        user:'root',
        password:'Bkav@2014',
        database:'food'
      }
    );
  
    connectsql.connect(function(err) {              
      if(err) {                                    
        console.log('error when connecting to db:', err);
        setTimeout(handleDisconnect, 3000);
      }                                    
    });                                     
                                            
    connectsql.on('error', function(err) {
      console.log('db error', err);
      if(err.code === 'PROTOCOL_CONNECTION_LOST') { 
        handleDisconnect();                        
      } else {                                      
        throw err;                                  
      }
    });
  }
  handleDisconnect()
  
  server.get('/p/:id', (req, res) => {
    const actualPage = '/post'
    const queryParams = { title: req.params.id } 
    app.render(req, res, actualPage, queryParams)
  })

  server.post('/sp_menu', (req, res) => {
    var title = req.body.title;
    connectsql.query("SELECT * FROM food.sanpham",(err,results)=>{
      if(err)
      {
        return res.send(err)
      }
      else {
        return  res.json({
          
          data: results
        })
      }
    });
  })

  server.post('/post', (req, res) => {
    var title = req.body.title;
    console.log(title)

    connectsql.query("SELECT * FROM food.sanpham where title='"+title+"'",(err,results)=>{
      if(err)
      {
        console.log(err)
        return res.send(err)
      }
      else {
        console.log(results)
        return  res.json({
          
          data: results
        })
      }
    });
  })

  server.get('*', (req, res) => {
    console.log('Ready on link port 3000');
    return handle(req, res)
  })

  server.listen(3000, (err) => {
    if (err) throw err
    console.log('> Ready on http://localhost:3000')
  })
})
.catch((ex) => {
  console.error(ex.stack)
  process.exit(1)
})