import React, { useState, useCallback } from 'react'
import Link from 'next/link'
import { Row, Col, Menu, Input, Icon } from 'antd'
import styled from 'styled-components';

// import bg from '/bg1.jpg';

const Nav = () => {
  const [activeTab, setActiveTab] = useState('home');
  const handleClickMenu = useCallback((e) => {
    setActiveTab(e.key);
  }, [activeTab])
  return (
    <div>
      <Header type="flex" justify="start" align="middle">
        <Col span={6} className="header-logo">
          logo
        </Col>
        {/* <Col span={18} className="header-menu">
          <Row type="flex" justify="start" align="middle"> */}
        <Col span={12}>
          <Menu onClick={handleClickMenu} style={{ background: "#ddd", borderLeft: '1px solid #c6c2c2' }} defaultSelectedKeys="home" selectedKeys={activeTab} mode="horizontal">
            <Menu.Item key="home">Trang Chủ</Menu.Item>
            <Menu.Item key="hot">Host</Menu.Item>
            <Menu.Item key="new">Hàng mới</Menu.Item>
            <Menu.Item key="best">Bán chạy nhất</Menu.Item>
            <Menu.Item key="km">Khuyến mại</Menu.Item>
          </Menu>
        </Col>
        <Col span={3} style={{}}>
          <Input prefix={<Icon type="search" />} />
        </Col>
        <Col span={3} style={{paddingLeft: '10px'}}>
          <span > <Icon type="phone" /> hot line</span>
        </Col>
        {/* </Row>
        </Col> */}
      </Header>
    </div>
  );
}

const Header = styled(Row)`
  /* height: 10px; */
  background: #ddd;

  position: relative;
  z-index: 10;
  max-width: 100%;
  /* background: #fff; */
  -webkit-box-shadow: 0 2px 8px #f0f1f2;
  box-shadow: 0 2px 8px #f0f1f2;
  /* color:  */
  /* background-image: url('/bg1.jpg'); */
`;

export default Nav
