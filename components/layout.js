import React from 'react'
import Head from 'next/head'
import Nav from './nav'

const Layout = (props) => (
	<div>
		<Head>
			<title>Home</title>
			<link rel="icon" href="/favicon.ico" />
		</Head>
		<Nav />
		{props.children}
	</div>
)

export default Layout
